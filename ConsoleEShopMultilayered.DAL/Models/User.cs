﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Models {
    public class User {

        private static int _nextId = 1;

        public int Id { get; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public decimal Balance { get; set; }
        //public List<OrderItem> Cart { get; set; }

        public User() {
            Id = _nextId++;
            IsAdmin = false;
            //Cart = new List<OrderItem>();
        }

        public override string ToString() {
            var role = IsAdmin ? "Admin" : "User";
            return $"User #{Id}, Email: {Email}, Password: {Password}, Role: {role}, Balance: {Balance}";
        }

    }
}
