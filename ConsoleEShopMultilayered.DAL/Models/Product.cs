﻿using System;

namespace ConsoleEShopMultilayered.DAL.Models {
    public class Product {

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }

        public static int NextId = 1;

        public Product(string name, string category, decimal price) {
            Id = NextId++;
            Name = name;
            Category = category;
            Price = price;
        }

        public override string ToString() => $"Product #{Id}: {Name}, Category: {Category}, Price: {Price}";
    }
}
