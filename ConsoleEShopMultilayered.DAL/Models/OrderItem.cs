﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Models {

    public class OrderItem {

        private static int _nextId = 1;

        public Product Product { get; set; }
        public int Count { get; set; }

        public OrderItem(Product product) {
            Product = product;
            Count = 1;
        }

        public OrderItem(Product product, int count) {
            Product = product;
            Count = count;
        }

        public override string ToString() => $"Product #{Product.Id} {Product.Name} Count: {Count}";
    }
}
