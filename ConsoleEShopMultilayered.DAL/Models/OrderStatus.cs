﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Models {
    public enum OrderStatus {
        New,
        CanceledByAdmin,
        PaymentReceived,
        Sent,
        Received,
        Complete,
        CanceledByUser
    }
}
