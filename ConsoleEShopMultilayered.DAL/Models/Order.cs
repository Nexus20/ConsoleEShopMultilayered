﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Models {
    public class Order {

        private static int _nextId = 1;

        public int Id { get; }
        public OrderStatus Status { get; set; }
        public int UserId { get; set; }
        public List<OrderItem> Items { get; set; }

        public decimal TotalPrice => Items.Sum(item => item.Product.Price * item.Count);

        //public decimal TotalPrice {
        //    get {

        //        DataContext.OrderItems.Where(oi => oi.)

        //    }
        //}

        public Order() {
            Id = _nextId++;
            Status = OrderStatus.New;
            Items = new List<OrderItem>();
        }

    }
}
