﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Repositories {
    public class UnitOfWork : IUnitOfWork {

        private IRepository<Order> _orderRepository;
        private IProductRepository _productRepository;
        private IUserRepository _userRepository;

        public IRepository<Order> Orders => _orderRepository ??= new OrderRepository();
        public IProductRepository Products => _productRepository ??= new ProductRepository();
        public IUserRepository Users => _userRepository ??= new UserRepository();
        

        private bool _disposed = false;

        public virtual void Dispose(bool disposing) {
            if (!_disposed) {
                if (disposing) {
                    // Dispose
                }

                _disposed = true;
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
