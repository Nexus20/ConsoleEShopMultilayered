﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Repositories {

    public class OrderRepository : IRepository<Order> {

        public IEnumerable<Order> GetAll() {
            return DataContext.Orders;
        }

        public Order Get(int id) {
            return DataContext.Orders.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Order> Find(Func<Order, bool> predicate) {
            return DataContext.Orders.Where(predicate).ToList();
        }

        public void Create(Order order) {
            DataContext.Orders.Add(order);
        }

        public void Delete(int id) {
            var order = DataContext.Orders.FirstOrDefault(p => p.Id == id);
            if (order == null) return;
            DataContext.Orders.Remove(order);
        }
    }
}
