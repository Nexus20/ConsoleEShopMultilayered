﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Repositories {

    public class ProductRepository : IProductRepository {
        
        public IEnumerable<Product> GetAll() {
            return DataContext.Products;
        }

        public Product Get(int id) {
            return DataContext.Products.FirstOrDefault(p => p.Id == id);
        }

        public Product Get(string name) {
            return DataContext.Products.FirstOrDefault(p => p.Name == name);
        }

        public IEnumerable<Product> Find(Func<Product, bool> predicate) {
            return DataContext.Products.Where(predicate).ToList();
        }

        public void Create(Product product) {
            DataContext.Products.Add(product);
        }

        public void Delete(int id) {
            var product = DataContext.Products.FirstOrDefault(p => p.Id == id);
            if (product == null) return;
            DataContext.Products.Remove(product);
        }
    }
}
