﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Repositories {

    public class UserRepository : IUserRepository {
        
        public IEnumerable<User> GetAll() {
            return DataContext.Users;
        }

        public User Get(int id) {
            return DataContext.Users.FirstOrDefault(p => p.Id == id);
        }

        public User Get(string email) {
            return DataContext.Users.FirstOrDefault(p => p.Email == email);
        }

        public IEnumerable<User> Find(Func<User, bool> predicate) {
            return DataContext.Users.Where(predicate).ToList();
        }

        public void Create(User user) {
            DataContext.Users.Add(user);
        }

        public void Delete(int id) {
            var user = DataContext.Users.FirstOrDefault(p => p.Id == id);
            if (user == null) return;
            DataContext.Users.Remove(user);
        }
    }
}
