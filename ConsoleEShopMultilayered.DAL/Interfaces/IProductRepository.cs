﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Interfaces {
    public interface IProductRepository : IRepository<Product> {
        Product Get(string name);
    }
}
