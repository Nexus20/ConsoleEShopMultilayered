﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Interfaces {
    public interface IUserRepository : IRepository<User> {
        User Get(string email);
    }
}
