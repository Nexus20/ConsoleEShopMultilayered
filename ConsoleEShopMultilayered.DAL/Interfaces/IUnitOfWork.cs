﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL.Interfaces {
    public interface IUnitOfWork : IDisposable {
        IRepository<Order> Orders { get; }
        IProductRepository Products { get; }
        IUserRepository Users { get; }
    }
}
