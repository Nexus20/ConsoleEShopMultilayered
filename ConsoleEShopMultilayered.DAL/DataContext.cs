﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.DAL {
    public class DataContext {

        public static readonly List<User> Users;
        public static readonly List<Product> Products;
        public static readonly List<Order> Orders;
        public static readonly List<OrderItem> OrderItems;

        static DataContext() {
            Users = new List<User>() {
                new User() {Email = "admin@admin.com", Password = "admin", IsAdmin = true, Balance = 100M},

                new User() {Email = "user1@user.com", Password = "user1", Balance = 10M},
                new User() {Email = "user2@user.com", Password = "user2", Balance = 10M},
                new User() {Email = "user3@user.com", Password = "user3", Balance = 0M},
                new User() {Email = "user4@user.com", Password = "user4", Balance = 0M},
            };

            Products = new List<Product>() {
                new Product("product1", "category1", 1),
                new Product("product2", "category1", 1),
                new Product("product3", "category2", 1),
                new Product("product4", "category2", 1),
            };

            Orders = new List<Order>() {
                new Order() {
                    Items = new List<OrderItem>() {
                        new OrderItem(Products[0], 2),
                        new OrderItem(Products[1], 1),
                    },
                    Status = OrderStatus.New,
                    UserId = 2
                },
                new Order() {
                    Items = new List<OrderItem>() {
                        new OrderItem(Products[1], 2),
                        new OrderItem(Products[2], 3),
                    },
                    Status = OrderStatus.CanceledByUser,
                    UserId = 2
                }
            };

            OrderItems = new List<OrderItem>();
        }

    }
}
