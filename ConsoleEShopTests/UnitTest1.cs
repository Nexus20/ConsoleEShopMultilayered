using System.Collections.Generic;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Services;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;
using ConsoleEShopMultilayered.DAL.Repositories;
using ConsoleEShopMultilayered.PL.Consoles;
using ConsoleEShopMultilayered.PL.Menus;
using ConsoleEShopMultilayered.PL.Options;
using Moq;
using NUnit.Framework;

namespace ConsoleEShopTests {
    public class Tests {
        [SetUp]
        public void Setup() {
        }

        [Test]
        public void ViewProductsList_ReturnsListOfProducts() {

            // Arrange

            IUnitOfWork uow = new UnitOfWork();

            var mock = new Mock<IProductRepository>();
            var sampleProducts = GetSampleProducts();
            Product.NextId = 1;
            mock.Setup(exp => exp.GetAll()).Returns(sampleProducts);

            var guestOptions = new GuestOptions(new TestConsoleWrapper(), new ProductService(uow), new UserService(uow), null);

            var expected = string.Join("\n", sampleProducts) + "\n";

            // Act
            var actual = guestOptions.ViewProductsList();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase("product1", 0)]
        [TestCase("product2", 1)]
        [TestCase("product3", 2)]
        [TestCase("product4", 3)]
        public void FindProductByName_ValidInput_ReturnsProduct(string name, int index) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var mock = new Mock<IProductRepository>();
            var sampleProducts = GetSampleProducts();
            Product.NextId = 1;
            mock.Setup(exp => exp.Get(name)).Returns(sampleProducts[index]);

            var guestOptions = new GuestOptions(new TestConsoleWrapper(new List<string>() { name }), new ProductService(uow), new UserService(uow), null);

            var expected = sampleProducts[index].ToString();

            // Act
            var actual = guestOptions.FindProductByName();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase("product5")]
        [TestCase("123")]
        [TestCase("9")]
        [TestCase("")]
        [TestCase(null)]
        public void FindProductByName_InvalidInput_ThrowsValidationException(string name) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var mock = new Mock<IProductRepository>();
            mock.Setup(exp => exp.Get(name)).Returns(default(Product));

            var guestOptions = new GuestOptions(new TestConsoleWrapper(new List<string>() { name }), new ProductService(uow), new UserService(uow), null);

            // Assert
            Assert.Throws<ValidationException>(() => guestOptions.FindProductByName());
        }

        [Test]
        [TestCase("user48@gg.com", "pass", "pass")]
        [TestCase("email@email.com", "pass", "pass")]
        [TestCase("emailWith@test.com", "123", "123")]
        public void Register_ValidInput_ReturnsRegisteredMessage(string email, string password, string passwordConfirm) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var guestOptions = new GuestOptions(new TestConsoleWrapper(new List<string>() { email, password, passwordConfirm }), new ProductService(uow), new UserService(uow), null);

            const string expected = "Registered";

            // Act
            var actual = guestOptions.Register();

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        [TestCase(null, "pass", "pass", "Missing user email")]
        [TestCase("email.com", "pass", "pass", "Incorrect email")]
        [TestCase("user1@user.com", "pass", "pass", "User already exists")]
        [TestCase("emailWith@test.com", "pass1", "pass2", "passwords don't match")]
        public void AddUser_InvalidInput_ReturnsErrorMessage(string email, string password, string passwordConfirm, string message) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var guestOptions = new GuestOptions(new TestConsoleWrapper(new List<string>() { email, password, passwordConfirm }), new ProductService(uow), new UserService(uow), null);

            var expected = message;

            //Act

            var actual = guestOptions.Register();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase("product1", 0)]
        [TestCase("product2", 1)]
        [TestCase("product3", 2)]
        [TestCase("product4", 3)]
        public void AddToCart_ValidInput_ReturnsSuccessMessage(string productName, int index) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var mock = new Mock<IProductRepository>();
            var sampleProducts = GetSampleProducts();
            Product.NextId = 1;
            mock.Setup(exp => exp.Get(productName)).Returns(sampleProducts[index]);

            var userOptions = new UserOptions(new TestConsoleWrapper(new List<string>() { productName }), new ProductService(uow), new UserService(uow), new OrderService(uow),  new UserDTO() {
                Id = GetSampleUsers()[1].Id,
                Balance = GetSampleUsers()[1].Balance,
                Cart = new List<OrderItemDTO>(),
                IsAdmin = GetSampleUsers()[1].IsAdmin,
                Email = GetSampleUsers()[1].Email,
                Password = GetSampleUsers()[1].Password
            }, null);

            const string expected = "Product added to cart";

            // Act
            var actual = userOptions.AddToCart();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(3)]
        public void CheckoutOrder_LowBalance_ReturnsErrorMessage(int index) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var sampleProducts = GetSampleProducts();
            Product.NextId = 1;
            var sampleUser = new UserDTO() {
                Id = GetSampleUsers()[index].Id,
                Email = GetSampleUsers()[index].Email,
                Password = GetSampleUsers()[index].Password,
                Balance = GetSampleUsers()[index].Balance,
                IsAdmin = GetSampleUsers()[index].IsAdmin,
                Cart = new List<OrderItemDTO>()
            };

            sampleUser.Cart = new List<OrderItemDTO>() {
                new OrderItemDTO(new ProductDTO() {
                    Id = sampleProducts[index].Id,
                    Category = sampleProducts[index].Category,
                    Name = sampleProducts[index].Name,
                    Price = sampleProducts[index].Price,
                }),
            };

            var userOptions = new UserOptions(new TestConsoleWrapper(), new ProductService(uow), new UserService(uow), new OrderService(uow), sampleUser, null);

            const string expected = "Not enough money! Top up your balance";

            // Act
            var actual = userOptions.CheckoutOrder();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(3)]
        public void CheckoutOrder_EmptyCart_ReturnsErrorMessage(int index) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var sampleUser = new UserDTO() {
                Id = GetSampleUsers()[index].Id,
                Email = GetSampleUsers()[index].Email,
                Password = GetSampleUsers()[index].Password,
                Balance = GetSampleUsers()[index].Balance,
                IsAdmin = GetSampleUsers()[index].IsAdmin,
                Cart = new List<OrderItemDTO>()
            };

            sampleUser.Cart = new List<OrderItemDTO>();

            var userOptions = new UserOptions(new TestConsoleWrapper(), new ProductService(uow), new UserService(uow), new OrderService(uow), sampleUser, null);

            const string expected = "Your cart is empty.";

            // Act
            var actual = userOptions.CheckoutOrder();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(0)]
        [TestCase(2)]
        public void CheckoutOrder_NormalBalance_ReturnsSuccessMessage(int index) {

            // Arrange
            IUnitOfWork uow = new UnitOfWork();

            var sampleProducts = GetSampleProducts();
            Product.NextId = 1;
            var sampleUser = new UserDTO() {
                Id = GetSampleUsers()[index].Id,
                Email = GetSampleUsers()[index].Email,
                Password = GetSampleUsers()[index].Password,
                Balance = GetSampleUsers()[index].Balance,
                IsAdmin = GetSampleUsers()[index].IsAdmin,
                Cart = new List<OrderItemDTO>()
            };

            sampleUser.Cart = new List<OrderItemDTO>() {
                new OrderItemDTO(new ProductDTO() {
                    Id = sampleProducts[index].Id,
                    Category = sampleProducts[index].Category,
                    Name = sampleProducts[index].Name,
                    Price = sampleProducts[index].Price,
                }),
            };

            var userOptions = new UserOptions(new TestConsoleWrapper(), new ProductService(uow), new UserService(uow), new OrderService(uow), sampleUser, null);

            const string expected = "Order created successful. Money withdrawn. \n Please let us know as soon as you receive it.";

            // Act
            var actual = userOptions.CheckoutOrder();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase("prod1", "cat1", "10")]
        [TestCase("prod2", "cat1", "0,8")]
        [TestCase("prod3", "cat2", "5")]
        [TestCase("prod4", "cat2", "4")]
        public void AddProduct_ValidInput_ShowsSuccessMessage(string name, string category, string price) {

            // Arrange

            IUnitOfWork uow = new UnitOfWork();

            var sampleUser = new UserDTO() {
                Id = GetSampleUsers()[0].Id,
                Email = GetSampleUsers()[0].Email,
                Password = GetSampleUsers()[0].Password,
                Balance = GetSampleUsers()[0].Balance,
                IsAdmin = GetSampleUsers()[0].IsAdmin,
                Cart = new List<OrderItemDTO>()
            };

            var adminOptions = new AdminOptions(new TestConsoleWrapper(new List<string>() { name, category, price }), new ProductService(uow), new UserService(uow), new OrderService(uow), sampleUser, null);

            var expected = $"Product #{name} was successfully added to DB";

            // Act

            var actual = adminOptions.AddNewProduct();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase("product1", "cat1", "10", "Product already exists")]
        [TestCase("", "cat1", "0,8", "Product name can't be null, empty, or whitespace")]
        [TestCase("prod3", "", "5", "Product category can't be null, empty, or whitespace")]
        [TestCase("prod4", "cat2", "0", "Incorrect price")]
        public void AddProduct_InvalidInput_ShowsErrorMessage(string name, string category, string price, string message) {

            // Arrange

            IUnitOfWork uow = new UnitOfWork();

            var sampleUser = new UserDTO() {
                Id = GetSampleUsers()[0].Id,
                Email = GetSampleUsers()[0].Email,
                Password = GetSampleUsers()[0].Password,
                Balance = GetSampleUsers()[0].Balance,
                IsAdmin = GetSampleUsers()[0].IsAdmin,
                Cart = new List<OrderItemDTO>()
            };

            var adminOptions = new AdminOptions(new TestConsoleWrapper(new List<string>() { name, category, price }), new ProductService(uow), new UserService(uow), new OrderService(uow), sampleUser, null);

            var expected = message;

            // Act

            var actual = adminOptions.AddNewProduct();

            // Assert
            Assert.AreEqual(expected, actual);

        }

        private List<Product> GetSampleProducts() =>
            new List<Product>() {
                new Product("product1", "category1", 1) {Id = 1},
                new Product("product2", "category1", 1) {Id = 2},
                new Product("product3", "category2", 1) {Id = 3},
                new Product("product4", "category2", 1) {Id = 4},
                new Product("prod1", "cat1", 10) {Id = 5},
                new Product("prod2", "cat1", 0.8M) {Id = 6},
                new Product("prod3", "cat2", 5) {Id = 7},
                new Product("prod4", "cat2", 4) {Id = 8},
            };

        private List<User> GetSampleUsers() =>
            new List<User>() {
                new User() {Email = "admin@admin.com", Password = "admin", IsAdmin = true, Balance = 100M },

                new User() {Email = "user1@user.com", Password = "user1", Balance = 0M },
                new User() {Email = "user2@user.com", Password = "user2", Balance = 10M },
                new User() {Email = "user3@user.com", Password = "user3", Balance = 0M },
                new User() {Email = "user4@user.com", Password = "user4", Balance = 10M },
            };
    }
}