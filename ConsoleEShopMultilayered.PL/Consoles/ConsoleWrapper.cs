﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL.Consoles {

    public class ConsoleWrapper : IConsole {

        public void Write(string message) {
            Console.Write(message);
        }

        public void WriteLine(string message) {
            Console.WriteLine(message);
        }

        public string ReadLine() {
            return Console.ReadLine();
        }

    }
}
