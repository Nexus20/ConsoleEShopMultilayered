﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL.Consoles {
    public interface IConsole {
        void Write(string message);
        void WriteLine(string message);
        string ReadLine();
    }
}
