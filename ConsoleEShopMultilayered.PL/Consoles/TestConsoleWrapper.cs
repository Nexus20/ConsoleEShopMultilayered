﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL.Consoles {
    public class TestConsoleWrapper : IConsole {

        private readonly List<string> _linesToRead;

        public TestConsoleWrapper() { }

        public TestConsoleWrapper(List<string> lines) {
            _linesToRead = lines;
        }

        public void Write(string message) { }

        public void WriteLine(string message) { }

        public string ReadLine() {
            var res = _linesToRead[0];
            _linesToRead.RemoveAt(0);
            return res;
        }

    }
}
