﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Models;
using ConsoleEShopMultilayered.PL.Consoles;
using ConsoleEShopMultilayered.PL.Options;

namespace ConsoleEShopMultilayered.PL.Menus {

    public class UserMenu : Menu {

        public UserDTO User { get; }

        public UserMenu(IConsole console, SelectionOptions options, UserDTO user) : base(console, options) {
            User = user;
        }

        public override Dictionary<string, Func<string>> CreateOptionsDictionary() {

            if (!(SelectionOptions is UserOptions userOptions)) {
                throw new ArgumentException("incorrect options!");
            }

            return new Dictionary<string, Func<string>> {
                {"1", userOptions.ViewProductsList},
                {"2", userOptions.FindProductByName},
                {"3", userOptions.AddToCart},
                {"4", userOptions.ShowCart},
                {"5", userOptions.ClearCart},
                {"6", userOptions.CheckoutOrder},
                {"7", userOptions.ViewOrdersHistory},
                {"8", userOptions.ReceiveOrder},
                {"9", userOptions.CancelOrder},
                {"10", userOptions.ChangeEmail},
                {"11", userOptions.ChangePassword},
                {"12", userOptions.CheckBalance},
                {"13", userOptions.TopUpBalance},
                {"14", userOptions.LogOut},
                {"15", Exit},
            };
        }

        public override void PrintOptions() {

            Console.WriteLine($"Hello, {User.Email}!\n Please select one of the options below\n");
            Console.WriteLine("1) View product list");
            Console.WriteLine("2) Find a product by name");
            Console.WriteLine("3) Add product to cart");
            Console.WriteLine("4) Show cart");
            Console.WriteLine("5) Clear cart");
            Console.WriteLine("6) Checkout order");
            Console.WriteLine("7) View orders history");
            Console.WriteLine("8) Receive order");
            Console.WriteLine("9) Cancel order");
            Console.WriteLine("10) Change email");
            Console.WriteLine("11) Change password");
            Console.WriteLine("12) Check balance");
            Console.WriteLine("13) Top up balance");
            Console.WriteLine("14) Log out");
            Console.WriteLine("15) Exit");

        }

    }
}
