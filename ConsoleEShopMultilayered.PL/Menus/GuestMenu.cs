﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.PL.Consoles;
using ConsoleEShopMultilayered.PL.Options;

namespace ConsoleEShopMultilayered.PL.Menus {

    public class GuestMenu : Menu {

        public GuestMenu(IConsole console, SelectionOptions options) : base(console, options) { }

        public override Dictionary<string, Func<string>> CreateOptionsDictionary() {

            if (!(SelectionOptions is GuestOptions guestOptions)) {
                throw new ArgumentException("incorrect options!");
            }

            return new Dictionary<string, Func<string>> {
                {"1", guestOptions.ViewProductsList},
                {"2", guestOptions.FindProductByName},
                {"3", guestOptions.LogIn},
                {"4", guestOptions.Register},
                {"5", Exit},
            };
        }

        public override void PrintOptions() {

            Console.WriteLine("Hello, guest!\n Please select one of the options below\n");
            Console.WriteLine("1) View product list");
            Console.WriteLine("2) Find a product by name");
            Console.WriteLine("3) Login");
            Console.WriteLine("4) Register");
            Console.WriteLine("5) Exit");

        }
    }
}
