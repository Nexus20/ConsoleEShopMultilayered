﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.PL.Consoles;
using ConsoleEShopMultilayered.PL.Options;

namespace ConsoleEShopMultilayered.PL.Menus {

    public class AdminMenu : Menu {

        public UserDTO User { get; }

        public AdminMenu(IConsole console, SelectionOptions options, UserDTO user) : base(console, options) {
            User = user;
        }

        public override Dictionary<string, Func<string>> CreateOptionsDictionary() {

            if (!(SelectionOptions is AdminOptions adminOptions)) {
                throw new ArgumentException("incorrect options!");
            }

            return new Dictionary<string, Func<string>> {
                {"1", adminOptions.ViewProductsList},
                {"2", adminOptions.FindProductByName},
                {"3", adminOptions.AddToCart},
                {"4", adminOptions.ShowCart},
                {"5", adminOptions.ClearCart},
                {"6", adminOptions.CheckoutOrder},
                {"7", adminOptions.ViewOrdersHistory},
                {"8", adminOptions.ReceiveOrder},
                {"9", adminOptions.CancelOrder},
                {"10", adminOptions.GetUsersInfo},
                {"11", adminOptions.ChangeUserEmail},
                {"12", adminOptions.ChangeUserPassword},
                {"13", adminOptions.ChangeOrderStatus},
                {"14", adminOptions.ChangeProductInfo},
                {"15", adminOptions.CheckBalance},
                {"16", adminOptions.AddNewProduct},
                {"17", adminOptions.TopUpBalance},
                {"18", adminOptions.LogOut},
                {"19", Exit},
            };
        }

        public override void PrintOptions() {

            Console.WriteLine($"Hello, {User.Email}!\n Please select one of the options below\n");
            Console.WriteLine("1) View product list");
            Console.WriteLine("2) Find a product by name");
            Console.WriteLine("3) Add product to cart");
            Console.WriteLine("4) Show cart");
            Console.WriteLine("5) Clear cart");
            Console.WriteLine("6) Checkout order");
            Console.WriteLine("7) View orders history");
            Console.WriteLine("8) Receive order");
            Console.WriteLine("9) Cancel order");
            Console.WriteLine("10) Get users info");
            Console.WriteLine("11) Change user email");
            Console.WriteLine("12) Change user password");
            Console.WriteLine("13) Change order status");
            Console.WriteLine("14) Change product info");
            Console.WriteLine("15) Check balance");
            Console.WriteLine("16) Add new product");
            Console.WriteLine("17) Top up balance");
            Console.WriteLine("18) Log out");
            Console.WriteLine("19) Exit");
        }

    }
}
