﻿using System;
using System.Collections.Generic;
using ConsoleEShopMultilayered.PL.Consoles;
using ConsoleEShopMultilayered.PL.Options;

namespace ConsoleEShopMultilayered.PL.Menus {
    public abstract class Menu {

        public bool IsActive { get; set; }

        protected SelectionOptions SelectionOptions;

        public abstract Dictionary<string, Func<string>> CreateOptionsDictionary();
        public abstract void PrintOptions();

        protected readonly IConsole Console;

        protected Menu(IConsole console, SelectionOptions options) {
            IsActive = true;
            Console = console;
            SelectionOptions = options;
        }

        public string HandleOptions() {
            PrintOptions();

            var option = Console.ReadLine()?.Trim();

            return ExecuteOption(option);
        }

        public string ExecuteOption(string option) {

            var options = CreateOptionsDictionary();

            return !options.ContainsKey(option) ? "Incorrect input" : options[option]();
        }

        public string Exit() {
            IsActive = false;
            return "Closing...";
        }

    }
}
