﻿using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.BLL.Services;
using Ninject.Modules;

namespace ConsoleEShopMultilayered.PL.Util {

    public class OrderModule : NinjectModule {

        public override void Load() {
            Bind<IOrderService>().To<OrderService>();
            Bind<IUserService>().To<UserService>();
            Bind<IProductService>().To<ProductService>();
        }
    }
}
