﻿using System;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.PL.Util;
using Ninject;

namespace ConsoleEShopMultilayered.PL {
    class Program {
        private static void Main(string[] args) {

            var kernel = new StandardKernel();
            kernel.Load(new OrderModule(), new ServiceModule());
            var orderService = kernel.Get<IOrderService>();
            var userService = kernel.Get<IUserService>();
            var productService = kernel.Get<IProductService>();

            var shop = new Shop(productService, orderService, userService);
            shop.Run();

            Console.ReadKey();
        }
    }
}
