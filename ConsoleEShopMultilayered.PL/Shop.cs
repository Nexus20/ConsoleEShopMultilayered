﻿using System;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.PL.Consoles;
using ConsoleEShopMultilayered.PL.Menus;
using ConsoleEShopMultilayered.PL.Options;

namespace ConsoleEShopMultilayered.PL {
    public class Shop {

        public Menu Menu { get; set; }
        private readonly ConsoleWrapper _console;
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public Shop(IProductService productService, IOrderService orderService, IUserService userService) {
            //IUnitOfWork uow = new UnitOfWork();
            //_console = new ConsoleWrapper();
            //_productService = new ProductService(uow);
            //_orderService = new OrderService(uow);
            //_userService = new UserService(uow);
            _console = new ConsoleWrapper();
            _productService = productService;
            _orderService = orderService;
            _userService = userService;
            Menu = new GuestMenu(_console, new GuestOptions(_console, _productService, _userService, LogIn));
        }

        public void Run() {

            while (Menu.IsActive) {
                Console.Clear();
                Console.WriteLine(Menu.HandleOptions());
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }

        }

        public void LogIn(UserDTO user) {

            if (user.IsAdmin) {

                Menu = new AdminMenu(_console, new AdminOptions(_console, _productService, _userService, _orderService, user, LogOut), user);

            }
            else {

                Menu = new UserMenu(_console, new UserOptions(_console, _productService, _userService, _orderService, user, LogOut), user);

            }
        }

        public void LogOut() {
            Menu = new GuestMenu(_console, new GuestOptions(_console, _productService, _userService, LogIn));
        }

    }
}
