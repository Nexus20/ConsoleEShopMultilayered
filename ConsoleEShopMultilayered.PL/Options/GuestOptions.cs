﻿using System.Collections.Generic;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.PL.Consoles;

namespace ConsoleEShopMultilayered.PL.Options {

    public class GuestOptions : SelectionOptions {

        private readonly IUserService _userService;

        public GuestOptions(IConsole console, IProductService productService, IUserService userService, LogInHandler onLogInAction) : base(console, productService) {
            _userService = userService;
            OnLogIn += onLogInAction;
        }

        public delegate void LogInHandler(UserDTO user);
        public event LogInHandler OnLogIn;

        public string LogIn() {
            try {
                Console.WriteLine("Please enter your email:");
                var email = Console.ReadLine()?.Trim();

                var user = _userService.GetUser(email);

                if (user == null) {
                    return "There are no such user";
                }

                Console.WriteLine("Please enter your password:");
                var password = Console.ReadLine();

                if (password != user.Password) {
                    return "Wrong password";
                }

                OnLogIn?.Invoke(user);
                return $"Hello, user {user.Email}";
            }
            catch (ValidationException e) {
                return e.Message;
            }
        }

        public string Register() {

            try {
                Console.WriteLine("Registering new account");

                Console.WriteLine("Please enter your email:");
                var email = Console.ReadLine()?.Trim();

                Console.WriteLine("Please enter password:");
                var password = Console.ReadLine();

                Console.WriteLine("Confirm password:");
                var passwordConfirm = Console.ReadLine();

                if (password != passwordConfirm) {
                    return "passwords don't match";
                }

                var newUser = new UserDTO() {
                    Balance = 0,
                    Cart = new List<OrderItemDTO>(),
                    Email = email,
                    IsAdmin = false,
                    Password = password,
                };

                _userService.AddUser(newUser);
                OnLogIn?.Invoke(newUser);
                return "Registered";
            }
            catch (ValidationException e) {
                return e.Message;
            }
        }

    }
}
