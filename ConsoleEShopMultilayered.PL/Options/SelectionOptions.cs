﻿using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.PL.Consoles;

namespace ConsoleEShopMultilayered.PL.Options {

    public abstract class SelectionOptions {

        protected IConsole Console;
        protected IProductService ProductService;

        protected SelectionOptions(IConsole console, IProductService productService) {
            Console = console;
            ProductService = productService;
        }

        protected string EnumerableToString<T>(IEnumerable<T> enumerable) {
            var sb = new StringBuilder();

            foreach (var item in enumerable) {
                sb.Append(item + "\n");
            }

            return sb.ToString();
        }

        public string ViewProductsList() {
            return EnumerableToString(ProductService.GetProducts());
        }

        public string FindProductByName() {

            Console.WriteLine("Please enter the name of the product you want to find");

            try {
                var productName = Console.ReadLine()?.Trim();
                return ProductService.GetProduct(productName).ToString();
            }
            catch (ValidationException e) {
                return e.Message;
            }

            //return product == null ? "There is no such product" : product.ToString();
        }
    }
}
