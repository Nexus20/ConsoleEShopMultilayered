﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;
using ConsoleEShopMultilayered.PL.Consoles;

namespace ConsoleEShopMultilayered.PL.Options {

    public class UserOptions : SelectionOptions {

        //private List<OrderItem> _cart;
        private readonly UserDTO _user;
        private readonly IUserService _userService;
        private readonly IOrderService _orderService;

        //public UserOptions(List<OrderItem> cart, User user) {
        public UserOptions(IConsole console, IProductService productsService, IUserService userService, IOrderService orderService, UserDTO user, LogOutHandler onLogOutAction) : base(console, productsService) {
            //_cart = cart;
            _user = user;
            _orderService = orderService;
            _userService = userService;
            OnLogOut += onLogOutAction;
        }

        public delegate void LogOutHandler();
        public event LogOutHandler OnLogOut;

        public string AddToCart() {

            Console.WriteLine("Please enter product name");

            var productName = Console.ReadLine()?.Trim();

            var product = ProductService.GetProduct(productName);

            if (product == null) {
                return "There is no such product in the store.";
            }

            _orderService.AddToCart(_user, product);

            //if (CheckProductInCart(product.Name)) {
            //    //var productInCart = _cart.First(item => item.Product.Name == product.Name);
            //    var productInCart = _user.Cart.First(item => item.Product.Name == product.Name);
            //    productInCart.Count++;
            //    return productInCart.ToString();
            //}

            //var newItem = new OrderItemDTO(product);
            ////_cart.Add(newItem);
            //_user.Cart.Add(newItem);
            //return newItem.ToString();
            return "Product added to cart";
        }

        //private bool CheckProductInCart(string productName) =>_cart.Any(item => item.Product.Name == productName);
        private bool CheckProductInCart(string productName) =>_user.Cart.Any(item => item.Product.Name == productName);

        public string ShowCart() {

            //if (_cart.Count == 0) {
            if (_user.Cart.Count == 0) {
                return "Cart is empty";
            }

            return EnumerableToString<OrderItemDTO>(_user.Cart);

            //var sb = new StringBuilder();

            ////foreach (var item in _cart) {
            //foreach (var item in _user.Cart) {
            //    sb.Append(item);
            //}

            //return sb.ToString();
        }

        public string CheckoutOrder() {

            //if (_cart.Count == 0) {
            if (_user.Cart.Count == 0) {
                return "Your cart is empty.";
            }

            var order = new OrderDTO() {
                //Items = _cart,
                Items = _user.Cart,
                UserId = _user.Id
            };

            if (order.TotalPrice > _user.Balance) {
                return "Not enough money! Top up your balance";
            }

            _orderService.AddOrder(_user, order);
            _user.Balance -= order.TotalPrice;

            return "Order created successful. Money withdrawn. \n Please let us know as soon as you receive it.";
        }

        public string CancelOrder() {

            Console.WriteLine("Enter order Id");

            if (!int.TryParse(Console.ReadLine(), out var id)) {
                return "Id isn't valid";
            }

            var order = _orderService.GetOrder(id);

            if (order == null) {
                return "There is no order with such id";
            }

            if (order.Status == OrderStatus.Received
                || order.Status == OrderStatus.Complete
                || order.Status == OrderStatus.CanceledByUser
                || order.Status == OrderStatus.CanceledByAdmin) {
                return "You can't cancel this order";
            }

            order.Status = OrderStatus.CanceledByUser;
            _user.Balance += order.TotalPrice;

            _orderService.UpdateStatus(id, OrderStatus.CanceledByUser);
            _userService.UpdateUser(_user);

            return $"Order #{id} cancelled. Your money returned";
        }

        public string CheckBalance() => $"Your balance is {_user.Balance}";

        public string TopUpBalance() {

            Console.WriteLine("Enter amount of money");

            if (!decimal.TryParse(Console.ReadLine(), out var money) || money <= 0) {
                return "Invalid money amount";
            }

            _user.Balance += money;
            _userService.UpdateUser(_user);
            return $"Your balance now is {_user.Balance}";
        }


        public string ClearCart() {
            _user.Cart = new List<OrderItemDTO>();
            return "Your cart is empty.";
        }

        public string ViewOrdersHistory() {

            var orders = _orderService.GetOrders(_user.Id);

            var sb = new StringBuilder();

            foreach (var item in orders) {
                sb.Append(item);
            }

            return sb.ToString();
        }

        public string ChangePassword() {
            Console.WriteLine("Input your old password:");
            var password = Console.ReadLine();

            if (password != _user.Password) {
                return "Wrong password!";
            }

            Console.WriteLine("Input your new password:");
            var newPassword = Console.ReadLine();

            if (password == newPassword) {
                return "Passwords must be different!";
            }

            _user.Password = newPassword;
            _userService.UpdateUser(_user);
            return "Password changed.";
        }

        public string ChangeEmail() {
            try {

                Console.WriteLine("Input your old password:");
                var password = Console.ReadLine();

                if (password != _user.Password) {
                    return "Wrong password!";
                }

                Console.WriteLine("Enter new email");

                var newEmail = Console.ReadLine();

                _user.Email = newEmail;
                _userService.UpdateUser(_user);
                return "Email changed";

            }
            catch (ValidationException e) {
                return e.Message;
            }
        }

        public string ReceiveOrder() {

            Console.WriteLine("Enter order Id");

            if (!int.TryParse(Console.ReadLine(), out var id)) {
                return "Id isn't valid";
            }

            var order = _orderService.GetOrder(id);

            if (order == null) {
                return "There is no order with such id";
            }

            if (order.Status == OrderStatus.Received
                || order.Status == OrderStatus.Complete
                || order.Status == OrderStatus.CanceledByUser
                || order.Status == OrderStatus.CanceledByAdmin) {
                return "You can't receive this order";
            }

            _orderService.UpdateStatus(id, OrderStatus.Received);

            return $"Status 'Received' set to order #{id}";
        }

        public string LogOut() {
            OnLogOut?.Invoke();
            return "Logging Out...";
        }
    }
}
