﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;
using ConsoleEShopMultilayered.PL.Consoles;

namespace ConsoleEShopMultilayered.PL.Options {

    public class AdminOptions : SelectionOptions {

        private readonly UserDTO _user;
        private readonly IUserService _userService;
        private readonly IOrderService _orderService;

        public AdminOptions(IConsole console, IProductService productService, IUserService userService, IOrderService orderService, UserDTO user, LogOutHandler onLogOutAction) : base(console, productService) {
            _user = user;
            _orderService = orderService;
            _userService = userService;
            OnLogOut += onLogOutAction;
        }

        public delegate void LogOutHandler();
        public event LogOutHandler OnLogOut;

        public string GetUsersInfo() {
            return EnumerableToString(_userService.GetUsers());
        }

        public string ChangeUserPassword() {

            Console.WriteLine("Input Id of the user:");

            if (!int.TryParse(Console.ReadLine(), out var userId) || userId <= 0) {
                return "Wrong Id";
            }

            var user = _userService.GetUser(userId);

            if (user == null) {
                return "There is no such user in DB!";
            }

            Console.WriteLine("Input user's new password:");
            var newPassword = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(newPassword)) {
                return "New password can't be null, empty or whitespace";
            }

            user.Password = newPassword;
            _userService.UpdateUser(user);
            return $"Password changed for user {user.Id}";
        }

        public string ChangeUserEmail() {

            try {

                Console.WriteLine("Input Id of the user:");

                if (!int.TryParse(Console.ReadLine(), out var userId) || userId <= 0) {
                    return "Wrong Id";
                }

                var user = _userService.GetUser(userId);

                Console.WriteLine("Enter new email");

                var newEmail = Console.ReadLine();

                if (_userService.UserExists(newEmail)) {
                    return "This email has been already taken!";
                }

                user.Email = newEmail;
                _userService.UpdateUser(user);
                return $"Email changed for the user #{user.Id}";
            }
            catch (ValidationException e) {
                return e.Message;
            }
        }

        public string AddNewProduct() {

            try {

                Console.WriteLine("Enter name of the new product");

                var productName = Console.ReadLine()?.Trim();

                Console.WriteLine("Enter name of the category");

                var productCategory = Console.ReadLine()?.Trim();

                Console.WriteLine("Enter price of the product");

                if (!decimal.TryParse(Console.ReadLine(), out var price) || price <= 0) {
                    return "Incorrect price";
                }

                var product = new ProductDTO() {
                    Category = productCategory,
                    Name = productName,
                    Price = price,
                };

                ProductService.AddProduct(product);

                return $"Product #{product.Name} was successfully added to DB";

            }
            catch (ValidationException e) {
                return e.Message;
            }
        }

        public string AddToCart() {

            Console.WriteLine("Please enter product name");

            var productName = Console.ReadLine()?.Trim();

            var product = ProductService.GetProduct(productName);

            if (product == null) {
                return "There is no such product in the store.";
            }

            _orderService.AddToCart(_user, product);

            return "Product added to cart";
        }

        public string ChangeProductInfo() {

            Console.WriteLine("Enter product id");

            if (!int.TryParse(Console.ReadLine(), out var productId) || productId <= 0) {
                return "Wrong product id";
            }

            var product = ProductService.GetProduct(productId);

            if (product == null) {
                return "There is no such product";
            }

            Console.WriteLine($"Product #{product.Id} {product.Name} {product.Price} from category {product.Category}");

            Console.WriteLine("1) Change name");
            Console.WriteLine("2) Change price");
            Console.WriteLine("3) Change category");

            if (!int.TryParse(Console.ReadLine(), out var option)) {
                return "Wrong option";
            }

            return option switch {
                1 => ChangeProductName(product),
                2 => ChangeProductPrice(product),
                3 => ChangeProductCategory(product),
                _ => "Wrong option"
            };
        }

        public string CancelOrder() {

            Console.WriteLine("Enter order Id");

            if (!int.TryParse(Console.ReadLine(), out var id)) {
                return "Id isn't valid";
            }

            if (_orderService.GetOrders(_user.Id).All(order => order.Id != id)) {
                return "There is no order with such id";
            }

            var order = _orderService.GetOrder(id);

            if (order.Status == OrderStatus.Received
                || order.Status == OrderStatus.Complete
                || order.Status == OrderStatus.CanceledByUser
                || order.Status == OrderStatus.CanceledByAdmin) {
                return "You can't cancel this order";
            }

            order.Status = OrderStatus.CanceledByUser;
            _user.Balance += order.TotalPrice;
            return $"Order #{id} cancelled. Your money returned";
        }

        public string ChangeOrderStatus() {
            Console.WriteLine("Input ID of the order:");

            if (!int.TryParse(Console.ReadLine(), out var orderId)) {
                return "Wrong order ID";
            }

            var order = _orderService.GetOrder(orderId);

            if (order == null) {
                return "There is no order with such ID!";
            }

            Console.WriteLine("Select new order status:\n 1) Canceled\n 3) Payment received\n 4) Sent\n 5) Complete\n):");

            if (!int.TryParse(Console.ReadLine(), out var option) || option < 1 || option > 5) {
                return "Wrong option";
            }

            order.Status = option switch {
                1 => OrderStatus.CanceledByAdmin,
                2 => OrderStatus.PaymentReceived,
                3 => OrderStatus.Sent,
                4 => OrderStatus.Complete,
            };

            _orderService.UpdateStatus(order.Id, order.Status);

            if (order.Status == OrderStatus.CanceledByAdmin) {
                var user = _userService.GetUser(order.UserId);
                user.Balance += order.TotalPrice;
                _userService.UpdateUser(user);
                return $"Order #{order.Id} canceled. Money returned to the client";
            }

            return $"Status of the order #{order.Id} changed.";
        }

        private string ChangeProductName(ProductDTO product) {

            try {

                Console.WriteLine("Enter new name of the product");

                var productName = Console.ReadLine()?.Trim();

                if (ProductService.ProductExists(productName)) {
                    return "Product with such name already exists";
                }

                var oldName = product.Name;
                product.Name = productName;
                ProductService.UpdateProduct(product);

                return $"Name of the product #{product.Id} changed to {productName} from {oldName}";
            }
            catch (ValidationException e) {
                return e.Message;
            }
        }

        private string ChangeProductPrice(ProductDTO product) {

            Console.WriteLine("Enter price of the product");

            if (!decimal.TryParse(Console.ReadLine(), out var price) || price <= 0) {
                return "Incorrect price";
            }

            product.Price = price;
            ProductService.UpdateProduct(product);
            return $"Price of the product #{product.Id} {product.Name} changed to {price}";
        }

        private string ChangeProductCategory(ProductDTO product) {

            Console.WriteLine("Enter category of the product");

            var productCategory = Console.ReadLine()?.Trim();

            if (string.IsNullOrWhiteSpace(productCategory)) {
                return "Product name can't be null, empty, or whitespace";
            }

            var oldCategory = product.Category;
            product.Category = productCategory;
            ProductService.UpdateProduct(product);
            return $"Category of the product #{product.Id} {product.Name} changed to {product.Category} from {oldCategory}";
        }

        private bool CheckProductInCart(string productName) =>_user.Cart.Any(item => item.Product.Name == productName);

        public string ShowCart() {

            if (_user.Cart.Count == 0) {
                return "Cart is empty";
            }

            var sb = new StringBuilder();

            foreach (var item in _user.Cart) {
                sb.Append(item);
            }

            return sb.ToString();
        }


        public string CheckoutOrder() {

            if (_user.Cart.Count == 0) {
                return "Your cart is empty.";
            }

            var order = new OrderDTO() {
                Items = _user.Cart,
                UserId = _user.Id
            };

            if (order.TotalPrice > _user.Balance) {
                return "Not enough money! Top up your balance";
            }

            if (order.TotalPrice > _user.Balance) {
                return "Not enough money! Top up your balance";
            }

            _orderService.AddOrder(_user, order);
            _user.Balance -= order.TotalPrice;

            return "Order created successful. Money withdrawn. \n Please let us know as soon as you receive it.";
        }

        public string CheckBalance() => $"Your balance is {_user.Balance}";

        public string TopUpBalance() {

            Console.WriteLine("Enter amount of money");

            if (!decimal.TryParse(Console.ReadLine(), out var money) || money <= 0) {
                return "Invalid money amount";
            }

            _user.Balance += money;
            _userService.UpdateUser(_user);
            return $"Your balance now is {_user.Balance}";
        }


        public string ClearCart() {
            _user.Cart = new List<OrderItemDTO>();
            return "Your cart is empty.";
        }

        public string ViewOrdersHistory() {

            var orders = _orderService.GetOrders(_user.Id);

            var sb = new StringBuilder();

            foreach (var item in orders) {
                sb.Append(item);
            }

            return sb.ToString();
        }

        public string ReceiveOrder() {

            Console.WriteLine("Enter order Id");

            if (!int.TryParse(Console.ReadLine(), out var id)) {
                return "Id isn't valid";
            }
            
            if (_orderService.GetOrders(_user.Id).All(order => order.Id != id)) {
                return "There is no order with such id";
            }

            _orderService.GetOrder(id).Status = OrderStatus.Received;
            return $"Status 'Received' set to order #{id}";
        }


        public string LogOut() {
            OnLogOut?.Invoke();
            return "Logging Out...";
        }
    }
}
