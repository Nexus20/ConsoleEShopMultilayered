﻿namespace ConsoleEShopMultilayered.BLL.DTO {

    public class OrderItemDTO {

        public ProductDTO Product { get; set; }
        public int Count { get; set; }

        public OrderItemDTO(ProductDTO product) {
            Product = product;
            Count = 1;
        }

        public OrderItemDTO(ProductDTO product, int count) {
            Product = product;
            Count = count;
        }

        public override string ToString() => $"Product {Product.Name} Count: {Count}";
    }
}
