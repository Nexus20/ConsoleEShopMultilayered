﻿namespace ConsoleEShopMultilayered.BLL.DTO {

    public class ProductDTO {

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }

        public override string ToString() => $"Product #{Id}: {Name}, Category: {Category}, Price: {Price}";
    }
}
