﻿using System.Collections.Generic;

namespace ConsoleEShopMultilayered.BLL.DTO {
    public class UserDTO {

        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public decimal Balance { get; set; }
        public List<OrderItemDTO> Cart { get; set; }

        public UserDTO() {
            IsAdmin = false;
            Cart = new List<OrderItemDTO>();
        }

        public override string ToString() {
            var role = IsAdmin ? "Admin" : "User";
            return $"User #{Id}, Email: {Email}, Password: {Password}, Role: {role}, Balance: {Balance}";
        }

    }
}
