﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.BLL.DTO {
    public class OrderDTO {

        public int Id { get; set; }
        public int UserId { get; set; }
        public OrderStatus Status { get; set; }

        public List<OrderItemDTO> Items { get; set; }

        public decimal TotalPrice => Items.Sum(item => item.Product.Price * item.Count);

        public OrderDTO() {
            Status = OrderStatus.New;
            Items = new List<OrderItemDTO>();
        }

        public override string ToString() {

            var sb = new StringBuilder();
            sb.Append($"Order #{Id}, Status {Status}, Details:\n");

            foreach (var item in Items) {
                sb.Append($"{item}\n");
            }

            return sb.ToString();
        }
    }
}
