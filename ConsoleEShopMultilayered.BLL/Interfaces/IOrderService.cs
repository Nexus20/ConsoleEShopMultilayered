﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.BLL.Interfaces {

    public interface IOrderService {
        OrderDTO GetOrder(int? id);
        void AddToCart(UserDTO userDto, ProductDTO productDto);
        void AddOrder(UserDTO userDto, OrderDTO orderDto);
        void UpdateStatus(int orderId, OrderStatus newStatus);
        IEnumerable<OrderDTO> GetOrders(int userId);
        void Dispose();
    }
}
