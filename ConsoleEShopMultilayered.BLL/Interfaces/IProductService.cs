﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces {

    public interface IProductService {
        void AddProduct(ProductDTO productDto);
        ProductDTO GetProduct(int? id);
        ProductDTO GetProduct(string name);
        IEnumerable<ProductDTO> GetProducts();
        //void ChangeProductName(int productId, string newName);
        //void ChangeProductCategory(int productId, string newCategory);
        //void ChangeProductPrice(int productId, decimal newPrice);
        void UpdateProduct(ProductDTO productDto);
        bool ProductExists(string name);
        void Dispose();
    }
}
