﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.BLL.DTO;

namespace ConsoleEShopMultilayered.BLL.Interfaces {

    public interface IUserService {
        UserDTO GetUser(int? id);
        UserDTO GetUser(string email);
        UserDTO AddUser(UserDTO user);
        bool UserExists(string email);
        IEnumerable<UserDTO> GetUsers();
        void UpdateUser(UserDTO user);
        void Dispose();
    }
}
