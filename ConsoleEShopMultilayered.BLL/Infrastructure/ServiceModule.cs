﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories;
using Ninject.Modules;

namespace ConsoleEShopMultilayered.BLL.Infrastructure {
    public class ServiceModule : NinjectModule {

        public override void Load() {
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
