﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.BLL.Services {

    public class ProductService : IProductService {

        public IUnitOfWork Database { get; set; }

        public ProductService(IUnitOfWork uow) {
            Database = uow;
        }

        public void AddProduct(ProductDTO productDto) {

            if (string.IsNullOrWhiteSpace(productDto.Name)) {
                throw new ValidationException("Product name can't be null, empty, or whitespace", "");
            }

            if (string.IsNullOrWhiteSpace(productDto.Category)) {
                throw new ValidationException("Product category can't be null, empty, or whitespace", "");
            }

            if (Database.Products.Get(productDto.Name) != null) {
                throw new ValidationException("Product already exists", "");
            }

            var product = new Product(productDto.Name, productDto.Category, productDto.Price);

            Database.Products.Create(product);
        }

        public IEnumerable<ProductDTO> GetProducts() {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Product>, List<ProductDTO>>(Database.Products.GetAll());
        }

        public ProductDTO GetProduct(int? id) {
            if (id == null)
                throw new ValidationException("Missing product id", "");
            var product = Database.Products.Get(id.Value);
            if (product == null)
                throw new ValidationException("Product not found", "");

            return new ProductDTO { Id = product.Id, Category = product.Category, Name = product.Name, Price = product.Price };
        }

        public ProductDTO GetProduct(string name) {
            if (string.IsNullOrWhiteSpace(name)) {
                throw new ValidationException("Missing product name", "");
            }

            var product = Database.Products.Get(name);

            if (product == null) {
                throw new ValidationException("Product not found", "");
            }

            return new ProductDTO { Id = product.Id, Category = product.Category, Name = product.Name, Price = product.Price };
        }

        public bool ProductExists(string name) {

            if (string.IsNullOrWhiteSpace(name)) {
                throw new ValidationException("Missing product name", "");
            }

            return Database.Products.GetAll().Any(p => p.Name == name);
        }

        public void UpdateProduct(ProductDTO productDto) {
            var product = Database.Products.Get(productDto.Id);
            product.Name = productDto.Name;
            product.Category = productDto.Category;
            product.Price = productDto.Price;
        }

        public void Dispose() {
            Database.Dispose();
        }
    }
}
