﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.BLL.Services {

    public class OrderService : IOrderService {

        public IUnitOfWork Database { get; set; }

        public OrderService(IUnitOfWork uow) {
            Database = uow;
        }

        public IEnumerable<OrderDTO> GetOrders(int userId) {

            var orderDtos = new List<OrderDTO>();
            var orders = Database.Orders.Find(o => o.UserId == userId).ToList();

            for (var i = 0; i < orders.Count; i++) {

                var orderDtoItems = new List<OrderItemDTO>();

                for (var j = 0; j < orders[i].Items.Count; j++) {

                    orderDtoItems.Add(new OrderItemDTO(new ProductDTO() {
                        Id = orders[i].Items[j].Product.Id,
                        Category = orders[i].Items[j].Product.Category,
                        Name = orders[i].Items[j].Product.Name,
                        Price = orders[i].Items[j].Product.Price,
                    }, orders[i].Items[j].Count));
                }

                orderDtos.Add(new OrderDTO() {
                    Id = orders[i].Id,
                    Status = orders[i].Status,
                    UserId = orders[i].UserId,
                    Items = orderDtoItems
                });
            }

            return orderDtos;
        }

        public OrderDTO GetOrder(int? id) {
            if (id == null) {
                throw new ValidationException("Missing order id", "");
            }

            var order = Database.Orders.Get(id.Value);
            if (order == null) {
                throw new ValidationException("Order not found", "");
            }

            var orderItemDtoList = new List<OrderItemDTO>();

            foreach (var item in order.Items) {
                orderItemDtoList.Add(new OrderItemDTO(
                    new ProductDTO() {
                        Id = item.Product.Id,
                        Category = item.Product.Category, 
                        Name = item.Product.Name, 
                        Price = item.Product.Price}, 
                    item.Count));
            }

            return new OrderDTO() { Id = order.Id, Items = orderItemDtoList, Status = order.Status, UserId = order.UserId };
        }

        public void AddToCart(UserDTO userDto, ProductDTO productDto) {

            var orderItem = userDto.Cart.Find(oi => oi.Product.Name == productDto.Name);
            if (orderItem == null) {
                userDto.Cart.Add(new OrderItemDTO(productDto));
            }
            else {
                orderItem.Count++;
            }
        }

        public void AddOrder(UserDTO userDto, OrderDTO orderDto) {

            var orderItems = new List<OrderItem>();

            foreach (var cartItem in userDto.Cart) {
                orderItems.Add(new OrderItem(new Product(
                    cartItem.Product.Name, 
                    cartItem.Product.Category, 
                    cartItem.Product.Price),
                    cartItem.Count));
            }

            var order = new Order() {
                Items = orderItems,
                UserId = userDto.Id
            };

            Database.Orders.Create(order);
            userDto.Cart = new List<OrderItemDTO>();
        }

        public void UpdateStatus(int orderId, OrderStatus newStatus) {
            Database.Orders.Get(orderId).Status = newStatus;
        }

        public void Dispose() {
            Database.Dispose();
        }
    }
}
