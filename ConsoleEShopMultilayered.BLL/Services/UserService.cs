﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Infrastructure;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Models;

namespace ConsoleEShopMultilayered.BLL.Services {

    public class UserService : IUserService {

        public IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork uow) {
            Database = uow;
        }

        public UserDTO AddUser(UserDTO userDto) {


            if (UserExists(userDto.Email)) {
                throw new ValidationException("User already exists", "");
            }

            var user = new User() {
                Balance = 0,
                //Cart = new List<(Product, int)>(),
                Email = userDto.Email,
                IsAdmin = false,
                Password = userDto.Password
            };

            Database.Users.Create(user);
            return userDto;
        }

        public IEnumerable<UserDTO> GetUsers() {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(Database.Users.GetAll());
        }

        public UserDTO GetUser(int? id) {

            if (id == null) {
                throw new ValidationException("Missing user id", "");
            }

            var user = Database.Users.Get(id.Value);
            if (user == null) {
                throw new ValidationException("User not found", "");
            }

            return new UserDTO() { Balance = user.Balance, Email = user.Email, IsAdmin = user.IsAdmin, Password = user.Password };
        }

        public UserDTO GetUser(string email) {

            if (string.IsNullOrWhiteSpace(email)) {
                throw new ValidationException("Missing user email", "");
            }

            if (!email.Contains('@')) {
                throw new ValidationException("Incorrect email", "");
            }

            var user = Database.Users.Get(email);

            if (user == null) {
                throw new ValidationException("User not found", "");
            }

            return new UserDTO() { Id = user.Id, Balance = user.Balance, Email = user.Email, IsAdmin = user.IsAdmin, Password = user.Password };
        }

        public bool UserExists(string email) {

            if (string.IsNullOrWhiteSpace(email)) {
                throw new ValidationException("Missing user email", "");
            }

            if (!email.Contains('@')) {
                throw new ValidationException("Incorrect email", "");
            }

            return Database.Users.GetAll().Any(u => u.Email == email);
        }

        public void UpdateUser(UserDTO userDto) {
            var user = Database.Users.Get(userDto.Id);
            user.Email = userDto.Email;
            user.Password = userDto.Password;
            user.Balance = userDto.Balance;
        }

        public void Dispose() {
            Database.Dispose();
        }
    }
}
